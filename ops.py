import bpy
from . import utils


class CUSTOM_UL_actions(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        # ~ self.use_filter_show = False
        r = layout.row()
        split = r.split(factor=0.12)
        split.prop(item, "selected", text="", emboss=True, translate=False)
        split.label(text=item.name, translate=False)


class ClearInvalidFcurves(bpy.types.Operator):
    """Clear invalid fcurves from selected actions"""
    bl_idname = "clear_invalid_fcurves.clear"
    bl_label = "Clear Invalid Fcurves"
    bl_options = {'UNDO'}

    include_actions: bpy.props.EnumProperty( name = "Include actions",
                                             items = ( ('CURRENT', "Current of selected objects", ""),
                                                       ('ALL_NLA', "All in NLA of selected objects", ""),
                                                       ('CUSTOM', "Custom", ""),
                                                       ('ALL', "All", ""), ),
                                             default = 'ALL_NLA' )

    report_deleted: bpy.props.BoolProperty(name="Report to console", default=False,
                            description="Report deleted fcurves to console")

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False

        row = layout.row()
        row.prop(self, "include_actions")

        if self.include_actions == 'CUSTOM':
            act_list = context.scene.clear_invalid_fcurves_settings.action_list
            row = layout.row()
            row.template_list( "CUSTOM_UL_actions", "",
                               act_list, "entries",
                               act_list, "index",
                               rows=2, maxrows=8 )

        row = layout.row()
        row.prop(self, "report_deleted")

    def update_action_list(self, context):
        act_list = context.scene.clear_invalid_fcurves_settings.action_list

        active_act = None
        if act_list.index >= 0 and act_list.index < len(act_list.entries):
            active_act = act_list.entries[act_list.index].name

        entries = []
        for a in bpy.data.actions:
            entry = act_list.entries.get(a.name, None)
            if entry is not None:
                selected = entry.selected
            else:
                selected = False
            entries.append((a.name, selected))

        act_list.entries.clear()
        act_list.index = -1

        for i, e in enumerate(entries):
            entry = act_list.entries.add()
            entry.name = e[0]
            if active_act is not None and entry.name == active_act:
                act_list.index = i
            entry.selected = e[1]

    def invoke(self, context, event):
        self.update_action_list(context)
        wm = context.window_manager
        return wm.invoke_props_dialog(self)  # width=600

    def execute(self, context):
        if not self.clear_fcurves(context):
            return {'CANCELLED'}
        return {'FINISHED'}

    def clear_fcurves(self, context):
        selected_actions_names = []
        if self.include_actions == 'CUSTOM':
            act_list = context.scene.clear_invalid_fcurves_settings.action_list
            selected_actions_names = [e.name for e in act_list.entries if e.selected]
        elif self.include_actions == 'ALL':
            act_list = context.scene.clear_invalid_fcurves_settings.action_list
            selected_actions_names = [e.name for e in act_list.entries]
        else:
            obs = utils.get_selected_objects(context, None)
            for ob in obs:
                selected_actions_names.extend( utils.get_object_actions_names(ob, self.include_actions) )

        selected_actions = utils.get_actions( selected_actions_names )

        for acc in selected_actions:
            to_remove = []

            for fc in acc.fcurves:
                # ~ print(fc.data_path)
                if (not fc.is_valid):
                    to_remove.append(fc)

            report = self.report_deleted and len(to_remove) > 0
            if report:
                print("Deleted fcurves from action \"{}\":".format(acc.name))

            for fc in to_remove:
                if report:
                    print("  {}[{}]".format(fc.data_path, fc.array_index))
                acc.fcurves.remove(fc)

            if report:
                print()
