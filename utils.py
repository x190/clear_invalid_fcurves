import bpy


def get_selected_objects(context, ob_types=None):
    selected = []
    for ob in context.view_layer.objects:
        if not ob.select_get() or ob.hide_get() or ob.hide_viewport:
            continue
        if (ob_types is not None) and (ob.type not in ob_types):
            continue
        selected.append(ob)
    return selected

def get_object_actions_names(ob, include_actions):
    assert( include_actions in {'ALL_NLA', 'CURRENT'} )
    actions_names = set()
    if ob.animation_data is not None:
        if ob.animation_data.action is not None:
            actions_names.add(ob.animation_data.action.name)
        if include_actions == 'ALL_NLA':
            for nla_track in ob.animation_data.nla_tracks:
                for s in nla_track.strips:
                    if s.action is not None:
                        actions_names.add(s.action.name)
    return actions_names

def get_actions(actions_names):
    actions = []
    added = set()
    for an in actions_names:
        if an in added:
            continue
        acc = bpy.data.actions.get(an, None)
        if acc is not None:
            actions.append(acc)
            added.add(an)
    return actions
