import bpy
from . import ops

bl_info = {
    "name": "Clear invalid fcurves",
    "author": "Xin",
    "version": (0, 1, 0),
    "blender": (2, 93, 0),
    "location": "",
    "description": "Removes invalid fcurves from selected actions",
    "doc_url": "",
    "tracker_url": "",
    "category": "Animation",
}

class ActionListEntry(bpy.types.PropertyGroup):
    selected: bpy.props.BoolProperty(name="Selected", default=False)

class ActionList(bpy.types.PropertyGroup):
    index: bpy.props.IntProperty(default=-1, min=-1, name="Active action")
    entries: bpy.props.CollectionProperty(type=ActionListEntry)

class ClearInvalidFcurvesProperties(bpy.types.PropertyGroup):
    action_list:  bpy.props.PointerProperty(type=ActionList)


def menu_func(self, context):
    layout = self.layout
    layout.separator()

    # ~ row = layout.row(align = True).split(factor=0.35)
    # ~ row.alignment = 'EXPAND'
    row = layout.row()
    row.operator_context = "INVOKE_DEFAULT"
    row.operator(ops.ClearInvalidFcurves.bl_idname, text="Clear Invalid Fcurves")
    # ~ row.operator("wm.call_menu", text="Apply rig scale...").name=ops_scale.ScaleArmaturesMenu.bl_idname


classes = (
    ActionListEntry,
    ActionList,
    ClearInvalidFcurvesProperties,

    ops.ClearInvalidFcurves,
)

def register():
    for c in classes:
        bpy.utils.register_class(c)
    bpy.types.VIEW3D_MT_object_animation.append(menu_func)
    bpy.types.Scene.clear_invalid_fcurves_settings = bpy.props.PointerProperty(type=ClearInvalidFcurvesProperties)

def unregister():
    del bpy.types.Scene.clear_invalid_fcurves_settings
    bpy.types.VIEW3D_MT_object_animation.remove(menu_func)
    for c in reversed(classes):
        bpy.utils.unregister_class(c)

if __name__ == "__main__":
    register()
